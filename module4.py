#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
import math
import collections
import sys, os
import operator
#Method that updates arrays that store players' names and stats
def updatePlayer(name, atBats, hits):
    index = -1
    for temp in players:
        if(temp['name'] ==name):
           index=players.index(temp)
    if(index == -1):
        player = {'name':name,'atBats':atBats,'hits':hits}
        players.append(player)
    else:
        currPlayer = players[index]
        currPlayer['atBats']+=atBats
        currPlayer['hits']+=hits

#http://stackoverflow.com/questions/15037803/how-does-a-user-input-a-filename
#above is citation of how to do some of the following
if len(sys.argv) == 1:
    print "You can also give filename as a command line argument"
    filename = raw_input("Enter Filename: ")
else:
    filename = sys.argv[1]

#http://stackoverflow.com/questions/9532499/check-whether-a-path-is-valid-in-python-without-creating-a-file-at-the-paths-ta
#above cites a way to do the following
if not os.path.exists(filename):
    sys.exit("I'm sorry, this Filename does not work. Please try again.")
    
f=open(filename, "r")


line=f.readline()
players = []
averages = {}
#the following is a loop that iterates through the baseball stats list (cardinals-1940.txt) and adds these items to an array owned by respective players.
while(line):
    regularExpression = "(?P<name>[\w\s]+)\sbatted\s(?P<atBats>\d)[\w\s]+with\s(?P<hits>\d)"
    scan=re.match(regularExpression, line)
    if scan:
        name = scan.group('name')
        atBat = (int)(scan.group('atBats'))
        hits = (int)(scan.group('hits'))
        updatePlayer(name, atBat, hits)
    line=f.readline()
    
#the following is a for loop that calculates batting avgs of the players. It adds the avgs an array.
for temp in players:
    battingAverage = round(float(temp['hits']) / temp['atBats'],3)
    averages[temp['name']] = battingAverage

#http://stackoverflow.com/questions/613183/sort-a-python-dictionary-by-value
#above it a citation of how to sort by value
#the following is a loop that prints out names and batting avgs.
for item in sorted(averages, key=averages.get, reverse=True):
    print item,':',averages[item]

f.close()
